# Created by Ashwini Bhargava
# Date - Apr 15 2018
import numpy as np
import cv2
#from matplotlib import pyplot as plt

#ROI
image = cv2.imread('track.jpg')
r = cv2.selectROI(image)
imCrop = image[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
target = cv2.imwrite("Im.jpg", imCrop)
hsv_crop = cv2.cvtColor(imCrop, cv2.COLOR_BGR2HSV)
cv2.imshow("Im", imCrop)
cv2.waitKey(0)

#GrabCut Algorithm
veil = np.zeros(image.shape[:2],np.uint8)
bgdModel = np.zeros((1,65),np.float64)
fgdModel = np.zeros((1,65),np.float64)
rect_lower = (100,200,1400, 1400) #lower half
#rect_upper = (0,0,1400, 400) #Upper half
newmask = cv2.imread('Im.jpg',0)
cv2.grabCut(image,veil,rect_lower,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
mask2 = np.where((veil==2)|(veil==0),0,1).astype('uint8')
image = image*mask2[:,:,np.newaxis]
cv2.imshow("Ima", image)
cv2.waitKey(0)
#plt.imshow(image)
#plt.colorbar()
#plt.show()

##To check dimensions of the image
#dimensions = image.shape
## height, width, number of channels in image
#height = image.shape[0]
#width = image.shape[1]
#channels = image.shape[2]
#print('Image Dimension : ',dimensions)
#print('Image Height : ',height)
#print('Image Width : ',width)
#print('Number of Channels : ',channels)

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, thres = cv2.threshold(gray, 127, 255, 0)
filter = cv2.GaussianBlur(image.copy(), (3, 3), 0)
edges = cv2.Canny(image, 25, 255)
hsv = cv2.cvtColor(filter, cv2.COLOR_BGR2HSV)

#define the range of white color
sensitivity = 56
lower_white = np.array([0,0,255-sensitivity])
upper_white = np.array([255, sensitivity, 255])

kdx = 0
mask = cv2.inRange(hsv, lower_white, upper_white)
res = cv2.bitwise_and(image, image, mask = mask)
kdx += 1
print('White =', kdx)

(_, cnts, _) = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
draw = cv2.drawContours(image, cnts, -1, (0, 255, 255), 4)

noofcircles = 0
noofrectangles = 0

for c in cnts:
    area = cv2.contourArea(c) #area of the contour
    perimeter = cv2.arcLength(c, True) #perimeter of the contour
    moment = cv2.moments(c) #moments of the image
    #centroid
    if moment['m00'] != 0:
        cx = int(moment['m01']/moment['m00'])
        cy = int(moment['m10']/moment['m00'])
    else:
        cx, cy = 0, 0
    centroid = [cx, cy]

    approx = cv2.approxPolyDP(c, 0.02 * perimeter, True) #contour approximation

    if len(approx) == 4:
        sq = cv2.drawContours(image, [c], 0, (0, 0, 255), -1)
        noofrectangles += 1
    else:
        cir = cv2.drawContours(image, [c], 0, (0, 255, 255), -1)
        noofcircles += 1

    hull = cv2.convexHull(c, False) #convex hull - checks a curve for convexity defects and corrects it
    x, y, w, h = cv2.boundingRect(c) #bounding rectangle around an image along the contours
    rectangle = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2) #Rectangle
    (x, y), radius = cv2.minEnclosingCircle(c)
    center = (int(x), int(y))
    radius = int(radius)
    circle = cv2.circle(image, center, radius, (0, 255, 0), 2) #Circle
    aspect_ratio = float(w)/h #Ratio of the width to height of the bounding rectangle of the object
    rect_area = w * h #area of the bounding rectangle
    extent = float(area)/rect_area #Ratio of contour area to bounding rectangle area
    hull_area = cv2.contourArea(hull) #Area of the convex hull
    equi_diameter = np.sqrt(4 * area/np.pi) #Diameter of the circle whose area is same as the contour area
    # orientation = (x,y),(MA,ma),angle = cv2.fitEllipse(c)
    # if (hull_area != 0):
    #     solidity = area/hull_area #Ratio of the contour area to its convex hull area
    # else:
    #     solidity = printf("ERROR!!!")

    print('Area = ', area, end='\n')
    print('Perimeter = ', perimeter, end='\n')
    print('Moments = ', moment, end='\n')
    print('Contour Approx = ', approx, end='\n')
    print('Convex Hull = ', hull, end='\n')
    print('Aspect Ratio = ', aspect_ratio, end='\n')
    print('Extent = ', extent, end='\n')
    print('Hull Area = ', hull_area, end='\n')
    print('Rect_area = ', rect_area)
    print('Centroid = ', centroid)
    print('No of rectangles =', noofrectangles)
    print('No of circles =', noofcircles)

#    x1 = bool(noofrectangles)
#    print("Rect_val = ", x1)
#    x2 = bool(noofcircles)
#    print("Circ_val = ", x2)
#    x3 = bool(center)
#    print("Loc_Circ = ", x3)
#    x4 = bool(x)
#    print("Loc_Rect = ", x4)
#    x5 = bool(kdx)
#    print("White = ", x5)
#    x6 = bool(imCrop.any())
#    print("Rep_pat = ", x6)

    cv2.imshow('Gray', gray)
    cv2.waitKey(0)
    cv2.imshow('Binary', thres)
    cv2.waitKey(0)
    cv2.imshow('Mask', res)
    cv2.waitKey(0)
    cv2.imshow('Contour', draw)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
